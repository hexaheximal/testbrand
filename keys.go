package testbrand

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

func CreateKeypair() (string, string, error) {
	key, err := rsa.GenerateKey(rand.Reader, 4096)

	if err != nil {
		return "", "", err
	}

	data, err := x509.MarshalPKCS8PrivateKey(key)

	if err != nil {
		return "", "", err
	}

	serializedPrivateKey := string(pem.EncodeToMemory(&pem.Block{
		Type:    "RSA PRIVATE KEY",
		Headers: map[string]string{},
		Bytes:   data,
	}))

	data, err = x509.MarshalPKIXPublicKey(&key.PublicKey)

	if err != nil {
		return "", "", err
	}

	serializedPublicKey := string(pem.EncodeToMemory(&pem.Block{
		Type:    "PUBLIC KEY",
		Headers: map[string]string{},
		Bytes:   data,
	}))

	return serializedPrivateKey, serializedPublicKey, nil
}