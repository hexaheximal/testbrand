package testbrand

const BrandName = "TestBrand"
const BrandSlug = "testbrand"
const LogoSVGURL = "/static/testbrand.svg"
const DefaultAvatar = "/static/testbrand_avatar.png"
const InstanceGroupAvatar = "/static/testbrand_instance_group_avatar.png"
const BrandCreditsHTML = "<p>[TestBrand credits]</p><p style=\"margin-top: 16px;\">The source code for TestBrand can be found <a href=\"https://codeberg.org/hexaheximal/testbrand\">here</a>.</p>"