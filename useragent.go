package testbrand

func GetUserAgent(app *App) string {
	return "Go-http-client/1.1 (" + BrandName + "/" + Version + "; +" + GetInstanceURL(app) + "/)"
}