package testbrand

import (
	"encoding/json"
	"fmt"
	"plugin"
)

type Module struct {
	Name        string
	Description string
	Version     string
	config      map[string]interface{}
	goPlugin    *plugin.Plugin
}

// ReadConfig() reads the module configuration into a struct pointer, p.
func (m *Module) ReadConfig(p any) error {
	// This is stupid and I love it.

	data, err := json.Marshal(m.config)

	if err != nil {
		return err
	}

	return json.Unmarshal(data, p)
}

// LoadModule() loads a module from the provided path.
func LoadModule(path string, app *App, config map[string]interface{}) (*Module, error) {
	module := new(Module)

	//
	// Load it with the Go plugin package
	// TODO: Implement custom module loaders
	//

	p, err := plugin.Open(path)

	if err != nil {
		return module, err
	}

	module.config = config
	module.goPlugin = p

	//
	// Load the module
	//

	s, err := p.Lookup("Load")

	if err != nil {
		return module, err
	}

	load, ok := s.(func(module *Module, app *App) error)

	if !ok {
		return module, fmt.Errorf("%s does not contain a valid Load function", path)
	}

	app.Modules = append(app.Modules, module)

	return module, load(module, app)
}