window.onload = () => {
	window.addEventListener("click", (e) => {
		if (!e.target.classList.contains("clickable-card")) {
			return;
		}

		location.href = e.target.attributes["data-url"].value;
	});
};