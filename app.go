package testbrand

import (
	"errors"
	"net/http"
	"time"

	mvc "codeberg.org/modularviewcontroller/modularviewcontroller"
	"codeberg.org/hexaheximal/testbrand/database"
	"github.com/google/uuid"
	"github.com/microcosm-cc/bluemonday"
)

type App struct {
	Database      database.Database
	MVCApp        mvc.App
	Config        Config
	Modules       []*Module
	Sessions      map[string]Session
	HTMLSanitizer *bluemonday.Policy
}

func (a *App) GetSession(r *mvc.Request) Session {
	sessionCookie, err := r.RawRequest.(*http.Request).Cookie("session")

	if err != nil {
		return Session{}
	}

	session, ok := a.Sessions[sessionCookie.Value]

	if !ok {
		return Session{}
	}

	return session
}

func (a *App) NewSession(user database.User) string {
	sessionID := uuid.New().String()

	if a.Sessions == nil {
		a.Sessions = make(map[string]Session)
	}

	a.Sessions[sessionID] = Session{
		ID:       sessionID,
		User:     user,
		LoggedIn: true,
		Settings: map[string]interface{}{},
	}

	return sessionID
}

func (a *App) DeleteSession(session Session) {
	if a.Sessions == nil {
		return
	}

	a.Sessions[session.ID] = Session{}
}

func (a *App) RedeemInviteCode(id string) error {
	invite, err := a.Database.GetInvite(id)

	if err != nil {
		return err
	}

	if invite.Uses >= invite.MaxUses {
		return errors.New("the provided invite code has expired")
	}

	invite.Uses += 1

	a.Database.UpdateInvite(invite)

	return nil
}

func (a *App) CreateInviteCode(creatorID string, maxUses int) (string, error) {
	invite := database.Invite{
		Created:   time.Now().UTC(),
		CreatorId: creatorID,
		Id:        uuid.New().String(),
		MaxUses:   maxUses,
		Uses:      0,
	}

	return invite.Id, a.Database.AddInvite(invite)
}