package testbrand

import (
	"net/http"
	"strings"

	mvc "codeberg.org/modularviewcontroller/modularviewcontroller"
)

//
// TODO: Set Content-Type to the correct JSON type for WebFinger.
//

func RegisterWebfingerRoute(app *App) {
	app.MVCApp.GET("/.well-known/webfinger", func(r *mvc.Request, w *mvc.ResponseWriter) {
		// TODO: Make this backend-agnostic
		resource := r.RawRequest.(*http.Request).URL.Query().Get("resource")

		if resource == "" || !strings.Contains(resource, "acct:") || !strings.Contains(resource, "@") {
			w.JSON(400, map[string]interface{}{
				"error": "Bad request",
			})

			return
		}

		address := strings.Split(resource[5:], "@")

		if len(address) != 2 {
			w.JSON(400, map[string]interface{}{
				"error": "Bad request",
			})

			return
		}

		if address[1] != app.Config.Host {
			// TODO: Maybe implement webfinger for remote actors in the future?
			w.JSON(400, map[string]interface{}{
				"error": "Bad request",
			})

			return
		}

		err := tryUser(address[0], app, w)

		if err != nil {
			err = tryGroup(address[0], app, w)

			if err != nil {
				w.JSON(404, map[string]interface{}{
					"error": "Not Found",
				})
			}
		}
	})
}

func tryUser(name string, app *App, w *mvc.ResponseWriter) error {
	_, err := app.Database.GetUser(name)

	if err != nil {
		return err
	}

	w.JSON(200, map[string]interface{}{
		"subject": "acct:" + name + "@" + app.Config.Host,
		"links": []map[string]interface{}{
			{
				"rel":  "self",
				"type": "application/activity+json",
				"href": GetURLFromName(app, name),
			},
		},
	})

	return nil
}

func tryGroup(name string, app *App, w *mvc.ResponseWriter) error {
	_, err := app.Database.GetGroup(name)

	if err != nil {
		return err
	}

	w.JSON(200, map[string]interface{}{
		"subject": "acct:" + name + "@" + app.Config.Host,
		"links": []map[string]interface{}{
			{
				"rel":  "self",
				"type": "application/activity+json",
				"href": GetURLFromName(app, name),
			},
		},
	})

	return nil
}