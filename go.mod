module codeberg.org/hexaheximal/testbrand

go 1.21

toolchain go1.22.2

require (
	codeberg.org/modularviewcontroller/modularviewcontroller v0.0.0-20240313235645-342b1e2b8da7
	github.com/google/uuid v1.6.0
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/microcosm-cc/bluemonday v1.0.26
	golang.org/x/crypto v0.21.0
	golang.org/x/term v0.18.0
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
