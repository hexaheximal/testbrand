package testbrand

import (
	"html/template"
	"io"
	"path/filepath"
)

func RenderTemplate(app *App, w io.Writer, name string, data any) error {
	t := template.New("root")

	_, err := t.ParseGlob(filepath.Join(app.Config.AssetsPath, "templates", "*"))

	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, name, data)

	if err != nil {
		return err
	}

	return nil
}