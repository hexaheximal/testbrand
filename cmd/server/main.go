package main

import (
	"bufio"
	"crypto/subtle"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	mvc "codeberg.org/modularviewcontroller/modularviewcontroller"
	"codeberg.org/hexaheximal/testbrand"
	"codeberg.org/hexaheximal/testbrand/database"
	"github.com/google/uuid"
	"github.com/microcosm-cc/bluemonday"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/term"
)

func main() {
	var config testbrand.Config

	configPath := filepath.Join(testbrand.DefaultConfigPath, "config.json")

	if os.Getenv("CONFIG_PATH") != "" {
		configPath = os.Getenv("CONFIG_PATH")
	}

	configData, err := os.ReadFile(configPath)

	if err != nil {
		err := os.MkdirAll(filepath.Dir(configPath), 0770)

		if err != nil {
			panic(err)
		}

		configData, err = json.MarshalIndent(testbrand.DefaultConfig, "", "\t")

		if err != nil {
			panic(err)
		}

		_ = os.WriteFile(configPath, configData, 0644)
	}

	err = json.Unmarshal(configData, &config)

	if err != nil {
		panic(err)
	}

	// TODO: PostgreSQL

	db, err := database.OpenDatabase(config.DatabaseType, config.DatabaseURI)

	if err != nil {
		panic(err)
	}

	users, err := testbrand.GetUserCount(&db)

	if err != nil {
		panic(err)
	}

	p := bluemonday.NewPolicy()

	p.AllowStandardURLs()

	p.AllowAttrs("href").OnElements("a")
	p.AllowElements("p")
	p.AllowElements("small")
	p.AllowElements("span")
	p.AllowElements("br")

	app := testbrand.App{
		Database:      db,
		MVCApp:        mvc.App{},
		Config:        config,
		Modules:       []*testbrand.Module{},
		HTMLSanitizer: p,
	}

	if users == 0 {
		initServer(app)
		return
	}

	startServer(app)
}

func startServer(app testbrand.App) {
	var backend mvc.NetHTTPBackend

	app.MVCApp.GET("/", func(r *mvc.Request, w *mvc.ResponseWriter) {
		w.SetHeader("Location", app.Config.LoggedOutHomepage)
		w.Status(307)
	})

	app.MVCApp.GET("/about", func(r *mvc.Request, w *mvc.ResponseWriter) {
		w.SetHeader("Content-Type", "text/html")
		w.Status(200)

		session := app.GetSession(r)

		testbrand.RenderTemplate(&app, w, "about.html", map[string]interface{}{
			"me":               session.User.Name,
			"brandName":        testbrand.BrandName,
			"brandSlug":        testbrand.BrandSlug,
			"logoSVGURL":       testbrand.LogoSVGURL,
			"instance":         app.Config.Host,
			"modules":          app.Modules,
			"brandCreditsHTML": template.HTML(testbrand.BrandCreditsHTML),
		})
	})

	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if r.URL != "/signup" {
			return true
		}

		if r.Method == "POST" {
			r.RawRequest.(*http.Request).ParseForm()

			username := r.RawRequest.(*http.Request).Form.Get("username")
			password := r.RawRequest.(*http.Request).Form.Get("password")
			confirmPassword := r.RawRequest.(*http.Request).Form.Get("confirmpassword")

			_, err := app.Database.GetUser(username)

			if err == nil {
				w.SetHeader("Content-Type", "text/html")
				w.Status(400)

				testbrand.RenderTemplate(&app, w, "signup.html", map[string]interface{}{
					"me":         "",
					"inviteonly": app.Config.InviteOnly,
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
					"error":      "There is already an account with the provided username",
				})

				return false
			}

			if 8 > len(password) {
				w.SetHeader("Content-Type", "text/html")
				w.Status(400)

				testbrand.RenderTemplate(&app, w, "signup.html", map[string]interface{}{
					"me":         "",
					"inviteonly": app.Config.InviteOnly,
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
					"error":      "Your password must be at least 8 characters long",
				})

				return false
			}

			if len(password) > 64 {
				w.SetHeader("Content-Type", "text/html")
				w.Status(400)

				testbrand.RenderTemplate(&app, w, "signup.html", map[string]interface{}{
					"me":         "",
					"inviteonly": app.Config.InviteOnly,
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
					"error":      "The length of your password cannot exceed 64 characters",
				})

				return false
			}

			if subtle.ConstantTimeCompare([]byte(password), []byte(confirmPassword)) != 1 {
				w.SetHeader("Content-Type", "text/html")
				w.Status(400)

				testbrand.RenderTemplate(&app, w, "signup.html", map[string]interface{}{
					"me":         "",
					"inviteonly": app.Config.InviteOnly,
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
					"error":      "The provided passwords do not match",
				})

				return false
			}

			if app.Config.InviteOnly {
				invite := r.RawRequest.(*http.Request).Form.Get("invite")

				err := app.RedeemInviteCode(invite)

				if err != nil {
					w.SetHeader("Content-Type", "text/html")
					w.Status(400)

					testbrand.RenderTemplate(&app, w, "signup.html", map[string]interface{}{
						"me":         "",
						"inviteonly": app.Config.InviteOnly,
						"brandName":  testbrand.BrandName,
						"brandSlug":  testbrand.BrandSlug,
						"logoSVGURL": testbrand.LogoSVGURL,
						"error":      "Invalid invite code",
					})

					return false
				}
			}

			//
			// Actually create the user
			//

			bcryptBytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)

			if err != nil {
				app.MVCApp.HandleError(500, r, w)
				return false
			}

			serializedPrivateKey, serializedPublicKey, err := testbrand.CreateKeypair()

			if err != nil {
				app.MVCApp.HandleError(500, r, w)
				return false
			}

			user := database.User{
				Created:     time.Now().UTC(),
				Description: "",
				Id:          uuid.New().String(),
				Name:        username,
				Password:    "bcrypt:" + string(bcryptBytes),
				PrivateKey:  serializedPrivateKey,
				PublicKey:   serializedPublicKey,
				Role:        "default",
			}

			err = app.Database.AddUser(user)

			if err != nil {
				app.MVCApp.HandleError(500, r, w)
				return false
			}

			sessionID := app.NewSession(user)

			w.SetHeader("Set-Cookie", "session="+sessionID+"; Secure; HttpOnly")
			w.SetHeader("Location", app.Config.LoggedInHomepage)
			w.Status(303)

			return true
		}

		if r.Method == "GET" {
			w.SetHeader("Content-Type", "text/html")
			w.Status(200)

			testbrand.RenderTemplate(&app, w, "signup.html", map[string]interface{}{
				"me":         "",
				"inviteonly": app.Config.InviteOnly,
				"brandName":  testbrand.BrandName,
				"brandSlug":  testbrand.BrandSlug,
				"logoSVGURL": testbrand.LogoSVGURL,
				"error":      "",
			})

			return false
		}

		return true
	})

	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if r.URL != "/login" {
			return true
		}

		if r.Method == "POST" {
			r.RawRequest.(*http.Request).ParseForm()

			username := r.RawRequest.(*http.Request).Form.Get("username")
			password := r.RawRequest.(*http.Request).Form.Get("password")

			user, err := app.Database.GetUser(username)

			if err != nil {
				w.SetHeader("Content-Type", "text/html")
				w.Status(200)

				testbrand.RenderTemplate(&app, w, "login.html", map[string]interface{}{
					"me":         "",
					"error":      "User not found",
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
				})

				return false
			}

			// TODO: Implement algorithms other than bcrypt

			if bcrypt.CompareHashAndPassword([]byte(user.Password[7:]), []byte(password)) != nil {
				w.SetHeader("Content-Type", "text/html")
				w.Status(200)

				testbrand.RenderTemplate(&app, w, "login.html", map[string]interface{}{
					"me":         "",
					"error":      "Incorrect password",
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
				})

				return false
			}

			sessionID := app.NewSession(user)

			w.SetHeader("Set-Cookie", "session="+sessionID+"; Secure; HttpOnly")
			w.SetHeader("Location", app.Config.LoggedInHomepage)
			w.Status(303)

			return false
		}

		if r.Method == "GET" {
			w.SetHeader("Content-Type", "text/html")
			w.Status(200)

			testbrand.RenderTemplate(&app, w, "login.html", map[string]interface{}{
				"me":         "",
				"brandName":  testbrand.BrandName,
				"brandSlug":  testbrand.BrandSlug,
				"logoSVGURL": testbrand.LogoSVGURL,
				"error":      "",
			})

			return false
		}

		return true
	})

	app.MVCApp.GET("/logout", func(r *mvc.Request, w *mvc.ResponseWriter) {
		app.DeleteSession(app.GetSession(r))
		w.SetHeader("Set-Cookie", "session=; expires=Thu, 01 Jan 1970 00:00:00 GMT; HttpOnly")
		w.SetHeader("Location", app.Config.LoggedOutHomepage)
		w.Status(307)
	})

	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if r.URL != "/create-invite" {
			return true
		}

		if r.Method != "GET" && r.Method != "POST" {
			return true
		}

		session := app.GetSession(r)

		if !session.LoggedIn {
			w.SetHeader("Location", "/login")
			w.Status(303)
			return false
		}

		if session.User.Role != "admin" {
			app.MVCApp.HandleError(403, r, w)
			return false
		}

		if r.Method == "POST" {
			r.RawRequest.(*http.Request).ParseForm()

			maxUses, err := strconv.Atoi(r.RawRequest.(*http.Request).Form.Get("maxuses"))

			if err != nil {
				w.SetHeader("Content-Type", "text/html")
				w.Status(400)

				testbrand.RenderTemplate(&app, w, "createinvite.html", map[string]interface{}{
					"me":         session.User.Name,
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
					"error":      "Invalid number of maximum uses",
					"inviteCode": "",
					"profileURL": testbrand.GetURLFromName(&app, session.User.Name),
				})

				return false
			}

			if maxUses > 65535 {
				w.SetHeader("Content-Type", "text/html")
				w.Status(400)

				testbrand.RenderTemplate(&app, w, "createinvite.html", map[string]interface{}{
					"me":         session.User.Name,
					"brandName":  testbrand.BrandName,
					"brandSlug":  testbrand.BrandSlug,
					"logoSVGURL": testbrand.LogoSVGURL,
					"error":      "Invalid number of maximum uses",
					"inviteCode": "",
					"profileURL": testbrand.GetURLFromName(&app, session.User.Name),
				})

				return false
			}

			inviteCode, err := app.CreateInviteCode(session.User.Id, maxUses)

			if err != nil {
				app.MVCApp.HandleError(500, r, w)
				return false
			}

			w.SetHeader("Content-Type", "text/html")
			w.Status(200)

			testbrand.RenderTemplate(&app, w, "createinvite.html", map[string]interface{}{
				"me":         session.User.Name,
				"brandName":  testbrand.BrandName,
				"brandSlug":  testbrand.BrandSlug,
				"logoSVGURL": testbrand.LogoSVGURL,
				"error":      "",
				"inviteCode": inviteCode,
				"profileURL": testbrand.GetURLFromName(&app, session.User.Name),
			})

			return false
		}

		if r.Method == "GET" {
			w.SetHeader("Content-Type", "text/html")
			w.Status(200)

			session := app.GetSession(r)

			testbrand.RenderTemplate(&app, w, "createinvite.html", map[string]interface{}{
				"me":         session.User.Name,
				"brandName":  testbrand.BrandName,
				"brandSlug":  testbrand.BrandSlug,
				"logoSVGURL": testbrand.LogoSVGURL,
				"error":      "",
				"inviteCode": "",
				"profileURL": testbrand.GetURLFromName(&app, session.User.Name),
			})

			return false
		}

		return true
	})

	testbrand.RegisterUserProfileRoute(&app)
	testbrand.RegisterGroupProfileRoute(&app)
	testbrand.RegisterEditUserProfileRoute(&app)

	if app.Config.EnableFederation {
		testbrand.RegisterUserActorRoute(&app)
		testbrand.RegisterGroupActorRoute(&app)
		testbrand.RegisterWebfingerRoute(&app)
	}

	app.MVCApp.SetErrorHandler(404, func(r *mvc.Request, w *mvc.ResponseWriter) {
		w.SetHeader("Content-Type", "text/html")
		w.Status(404)

		session := app.GetSession(r)

		testbrand.RenderTemplate(&app, w, "404.html", map[string]interface{}{
			"me":         session.User.Name,
			"brandName":  testbrand.BrandName,
			"brandSlug":  testbrand.BrandSlug,
			"logoSVGURL": testbrand.LogoSVGURL,
		})
	})

	// Load modules

	modulePath, err := filepath.Abs(app.Config.ModulePath)

	if err != nil {
		panic(err)
	}

	files, err := os.ReadDir(modulePath)

	if err != nil {
		panic(err)
	}

	for _, file := range files {
		if !file.IsDir() && strings.HasSuffix(file.Name(), ".so") {
			absolutePath := filepath.Join(modulePath, file.Name())

			log.Println("Loading module:", absolutePath)

			moduleConfig := map[string]interface{}{}

			userModuleConfig, ok := app.Config.ModuleOptions[strings.TrimSuffix(file.Name(), filepath.Ext(file.Name()))]

			if ok {
				moduleConfig = userModuleConfig
			}

			_, err := testbrand.LoadModule(absolutePath, &app, moduleConfig)

			if err != nil {
				panic(err)
			}
		}
	}

	backend.Launch(app.MVCApp, app.Config.Port)
}

func initServer(app testbrand.App) {
	fmt.Println("===== Welcome to " + testbrand.BrandName + "! =====")

	fmt.Print("Please enter the host (the domain number or IP including the custom port number if applicable, e.g. example.com, localhost:8080, etc.) for your new instance: ")

	r := bufio.NewReader(os.Stdin)

	host, err := r.ReadString('\n')

	if err != nil {
		panic(err)
	}

	host = strings.TrimSpace(host)

	fmt.Print("Please enter the directory where modules are stored (blank for " + filepath.Join(testbrand.DefaultConfigPath, "modules") + "): ")

	r = bufio.NewReader(os.Stdin)

	requestedModulePath, err := r.ReadString('\n')

	if err != nil {
		panic(err)
	}

	requestedModulePath = strings.TrimSpace(requestedModulePath)

	fmt.Print("Please enter the username for your new admin account: ")

	r = bufio.NewReader(os.Stdin)

	username, err := r.ReadString('\n')

	if err != nil {
		panic(err)
	}

	username = strings.TrimSpace(username)

	fmt.Print("Please enter the password for your new admin account: ")

	passwordBytes, err := term.ReadPassword(int(os.Stdin.Fd()))

	if err != nil {
		panic(err)
	}

	fmt.Println("\n\nSetting up your new account...")

	bcryptBytes, err := bcrypt.GenerateFromPassword(passwordBytes, 10)

	if err != nil {
		panic(err)
	}

	serializedPrivateKey, serializedPublicKey, err := testbrand.CreateKeypair()

	if err != nil {
		panic(err)
	}

	app.Database.AddUser(database.User{
		Created:     time.Now().UTC(),
		Description: "",
		Id:          uuid.New().String(),
		Name:        username,
		Password:    "bcrypt:" + string(bcryptBytes),
		PrivateKey:  serializedPrivateKey,
		PublicKey:   serializedPublicKey,
		Role:        "admin",
	})

	fmt.Println("\nSetting up the instance group...")

	serializedPrivateKey, serializedPublicKey, err = testbrand.CreateKeypair()

	if err != nil {
		panic(err)
	}

	app.Database.AddGroup(database.Group{
		Created:     time.Now().UTC(),
		Description: "",
		Id:          uuid.New().String(),
		Name:        host,
		PrivateKey:  serializedPrivateKey,
		PublicKey:   serializedPublicKey,
	})

	app.Config.Host = host

	// Automatically enable plaintext HTTP mode if the specified host is a local one

	if host == "localhost" || host == "127.0.0.1" || strings.HasPrefix(host, "localhost:") || strings.HasPrefix(host, "127.0.0.1:") {
		app.Config.PlaintextHTTPMode = true
	}

	if requestedModulePath != "" {
		app.Config.ModulePath = requestedModulePath
	}

	configData, err := json.MarshalIndent(app.Config, "", "\t")

	if err != nil {
		panic(err)
	}

	configPath := filepath.Join(testbrand.DefaultConfigPath, "config.json")

	if os.Getenv("CONFIG_PATH") != "" {
		configPath = os.Getenv("CONFIG_PATH")
	}

	err = os.WriteFile(configPath, configData, 0644)

	if err != nil {
		panic(err)
	}

	fmt.Println("\nFinished! Run this program again to start the server.")
}