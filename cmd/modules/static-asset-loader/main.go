package main

import (
	"io"
	"mime"
	"os"
	"path/filepath"
	"strings"

	mvc "codeberg.org/modularviewcontroller/modularviewcontroller"
	"codeberg.org/hexaheximal/testbrand"
)

func Load(module *testbrand.Module, app *testbrand.App) error {
	module.Name = "Static Asset Loader"
	module.Description = "Loads static assets. Useful when there is no reverse-proxy server involved."
	module.Version = testbrand.Version

	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if !strings.HasPrefix(r.URL, "/static") {
			return true
		}

		path := filepath.Join(app.Config.AssetsPath, "static", strings.Replace(r.URL, "/static", "", 1))

		_, err := os.Stat(path)

		if err == nil || os.IsExist(err) {
			f, err := os.Open(path)

			if err == nil {
				defer f.Close()

				w.SetHeader("Content-Type", mime.TypeByExtension(filepath.Ext(path)))
				w.Status(200)
				io.Copy(w, f)

				return false
			}
		}

		return true
	})

	return nil
}