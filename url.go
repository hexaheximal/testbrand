package testbrand

func GetInstanceURL(app *App) string {
	output := "https://"

	if app.Config.PlaintextHTTPMode {
		output = "http://"
	}

	output += app.Config.Host

	return output
}

func GetURLFromName(app *App, name string) string {
	output := GetInstanceURL(app)
	output += "/@"
	output += name

	return output
}

func GetKeyURLFromName(app *App, name string) string {
	output := GetInstanceURL(app)
	output += "/@"
	output += name
	output += "#main-key"

	return output
}

func GetInboxURLFromName(app *App, name string) string {
	output := GetInstanceURL(app)
	output += "/inbox/actor/"
	output += name

	return output
}

func GetOutboxURLFromName(app *App, name string) string {
	output := GetInstanceURL(app)
	output += "/outbox/actor/"
	output += name

	return output
}

func GetAvatarURL(app *App, server string, name string) string {
	if server == app.Config.Host || server == "" {
		server = "local"
	}

	return GetInstanceURL(app) + "/storage/avatar/" + server + "/" + name
}