package testbrand

import "path/filepath"

type Config struct {
	PlaintextHTTPMode bool                              `json:"plaintext_http_mode"`
	Host              string                            `json:"host"`
	Port              int                               `json:"port"`
	ModulePath        string                            `json:"module_path"`
	AssetsPath        string                            `json:"assets_path"`
	DatabaseType      string                            `json:"database_type"`
	DatabaseURI       string                            `json:"database_uri"`
	LoggedOutHomepage string                            `json:"logged_out_homepage"`
	LoggedInHomepage  string                            `json:"logged_in_homepage"`
	InviteOnly        bool                              `json:"invite_only"`
	EnableFederation  bool                              `json:"enable_federation"`
	ModuleOptions     map[string]map[string]interface{} `json:"module_options"`
}

var DefaultConfig = Config{
	PlaintextHTTPMode: false,
	Host:              "example.com",
	Port:              8080,
	ModulePath:        filepath.Join(DefaultConfigPath, "modules"),
	AssetsPath:        "assets",
	DatabaseType:      "sqlite3",
	DatabaseURI:       filepath.Join(DefaultConfigPath, BrandSlug+".db"),
	LoggedOutHomepage: "/about",
	LoggedInHomepage:  "/about",
	InviteOnly:        true,
	EnableFederation:  false,
	ModuleOptions:     map[string]map[string]interface{}{},
}