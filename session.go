package testbrand

import "codeberg.org/hexaheximal/testbrand/database"

type Session struct {
	ID       string
	User     database.User
	LoggedIn bool
	Settings map[string]interface{}
}