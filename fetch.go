package testbrand

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

// Headers is a shortcut for map[string]string.
// As the name implies, it is used to store HTTP headers.
type Headers map[string]string

// ActivityGet() provides a simple interface for fetching ActivityPub objects.
func ActivityGet(app *App, url string) (int, map[string]interface{}, error) {
	log.Println("GET (ActivityPub)", url)

	outputMap := make(map[string]interface{})

	response, err := CustomFetch(app, "GET", url, nil, "application/activity+json", Headers{})

	if err != nil {
		return 0, outputMap, err
	}

	responseBytes, err := io.ReadAll(response.Body)

	if err != nil {
		return response.StatusCode, outputMap, err
	}

	response.Body.Close()

	err = json.Unmarshal(responseBytes, &outputMap)

	if err != nil {
		return response.StatusCode, outputMap, err
	}

	return response.StatusCode, outputMap, nil
}

func ActivityPost(app *App, postURL string, keyID string, privateKey *rsa.PrivateKey, data any) error {
	var buf bytes.Buffer

	log.Println("POST (ActivityPub)", postURL)

	parsedURL, err := url.Parse(postURL)

	if err != nil {
		return err
	}

	serializedData, err := json.Marshal(data)

	if err != nil {
		return err
	}
	_, err = buf.Write(serializedData)

	if err != nil {
		return err
	}

	dateString := time.Now().UTC().Format(http.TimeFormat)

	h := sha256.Sum256([]byte(serializedData))

	var digestBuf bytes.Buffer

	e := base64.NewEncoder(base64.StdEncoding, &digestBuf)

	_, err = e.Write(h[:])

	if err != nil {
		return err
	}

	e.Close()

	digestString := "SHA-256=" + digestBuf.String()

	h = sha256.Sum256([]byte("(request-target): post " + parsedURL.Path + "\nhost: " + parsedURL.Host + "\ndate: " + dateString + "\ndigest: " + digestString))

	sig, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, h[:])

	if err != nil {
		return err
	}

	var sigBuf bytes.Buffer

	e = base64.NewEncoder(base64.StdEncoding, &sigBuf)

	_, err = e.Write(sig)

	if err != nil {
		return err
	}

	e.Close()

	_, err = CustomFetch(app, "POST", postURL, &buf, "application/activity+json", Headers{
		"Signature": "keyId=\"" + keyID + "\",headers=\"(request-target) host date digest\",signature=\"" + sigBuf.String() + "\"",
		"Host":      parsedURL.Host,
		"Date":      dateString,
		"Digest":    digestString,
	})

	return err
}

func Get(app *App, url string, accept string) (int, []byte, error) {
	log.Println("GET", url)

	response, err := CustomFetch(app, "GET", url, nil, accept, Headers{})

	if err != nil {
		return 0, []byte{}, err
	}

	responseBytes, err := io.ReadAll(response.Body)

	if err != nil {
		return response.StatusCode, []byte{}, err
	}

	response.Body.Close()

	return response.StatusCode, responseBytes, nil
}

// A powerful fetch function that is designed to allow for complex use.
// Don't use this unless you actually need it. Use one of the wrapper functions instead.
func CustomFetch(app *App, method string, fetchURL string, body io.Reader, accept string, headers Headers) (*http.Response, error) {
	parsedURL, err := url.Parse(fetchURL)

	if err != nil {
		return nil, err
	}

	// TODO: Use the config file instead of environment variables (especially since this is a security thing)

	if os.Getenv("DISABLE_FETCH") == "true" {
		return nil, errors.New("outbound network requests are disabled on this instance")
	}

	if os.Getenv("RESTRICTED_FETCH") == "true" {
		allowedHosts := strings.Split(os.Getenv("ALLOWED_FETCH_HOSTS"), ",")
		isAllowed := false

		for _, v := range allowedHosts {
			if v == parsedURL.Host {
				isAllowed = true
				break
			}
		}

		if !isAllowed {
			return nil, errors.New("this instance has disabled outbound requests for " + parsedURL.Host)
		}
	}

	request, err := http.NewRequest(method, fetchURL, body)

	if err != nil {
		return nil, err
	}

	if accept != "" {
		request.Header.Add("Accept", accept)
	}

	request.Header.Add("User-Agent", GetUserAgent(app))

	for key, value := range headers {
		request.Header.Add(key, value)
	}

	return http.DefaultClient.Do(request)
}