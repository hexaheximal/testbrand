package testbrand

import (
	"strings"

	mvc "codeberg.org/modularviewcontroller/modularviewcontroller"
)

func RegisterGroupProfileRoute(app *App) {
	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if !strings.HasPrefix(r.URL, "/@") {
			return true
		}

		name := r.URL[2:]

		if strings.Contains(name, "/") {
			return true
		}

		group, err := app.Database.GetGroup(name)

		if err != nil {
			return true
		}

		w.SetHeader("Content-Type", "text/html")
		w.Status(200)

		iconURL := DefaultAvatar

		if name == app.Config.Host {
			iconURL = InstanceGroupAvatar
		}

		session := app.GetSession(r)

		RenderTemplate(app, w, "profile.html", map[string]interface{}{
			"me":          session.User.Name,
			"brandName":   BrandName,
			"brandSlug":   BrandSlug,
			"logoSVGURL":  LogoSVGURL,
			"type":        "Group",
			"name":        group.Name,
			"description": app.HTMLSanitizer.Sanitize(group.Description),
			"iconURL":     iconURL, // TODO: Implement profile avatars
			"created":     group.Created.Format("January 2, 2006"),
		})

		return false
	})
}

func RegisterGroupActorRoute(app *App) {
	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if !strings.HasPrefix(r.URL, "/@") {
			return true
		}

		name := r.URL[2:]

		if strings.Contains(name, "/") {
			return true
		}

		if !strings.Contains(r.Headers.Get("Accept"), "json") {
			return true
		}

		group, err := app.Database.GetGroup(name)

		if err != nil {
			return true
		}

		iconURL := "/static/placeholder.png"

		if name == app.Config.Host {
			iconURL = "/static/instanceGroup.png"
		}

		w.JSON(200, map[string]interface{}{
			"@context": []string{
				"https://www.w3.org/ns/activitystreams",
				"https://w3id.org/security/v1",
			},
			"id":                GetURLFromName(app, name),
			"type":              "Group",
			"preferredUsername": name,
			"summary":           group.Description,
			"inbox":             GetInboxURLFromName(app, name),
			"icon":              iconURL,
			"publicKey": map[string]interface{}{
				"id":           GetKeyURLFromName(app, name),
				"owner":        GetURLFromName(app, name),
				"publicKeyPem": group.PublicKey, // TODO: key data
			},
		})

		return false
	})
}