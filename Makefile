.SECONDEXPANSION:
O ?= bin
GO ?= go

BRAND_NAME ?= TestBrand
BRAND_SLUG ?= testbrand

MODULES ?= static-asset-loader
MODULE_TARGETS = $(MODULES:%=$(O)/modules/%.so)

all: $(O)/$(BRAND_SLUG) $(MODULE_TARGETS)

$(O)/$(BRAND_SLUG): $(wildcard cmd/server/*) $(wildcard *.go) $(wildcard */*.go)
	$(GO) build $(EXTRA_GOFLAGS) -o "$@" cmd/server/main.go

$(O)/modules/%.so: $$(wildcard cmd/modules/%/*) $(wildcard *.go) $(wildcard */*.go)
	$(GO) build $(EXTRA_GOFLAGS) -buildmode=plugin -o "$@" "$(@:$(O)/modules/%.so=cmd/modules/%/main.go)"

clean:
	rm -r "$(O)"