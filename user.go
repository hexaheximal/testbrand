package testbrand

import (
	"html/template"
	"net/http"
	"strings"

	mvc "codeberg.org/modularviewcontroller/modularviewcontroller"
	"codeberg.org/hexaheximal/testbrand/database"
)

func GetUserCount(db *database.Database) (int, error) {
	users := 0

	err := db.QueryRow("SELECT COUNT(*) as count FROM users;", []any{}, []any{&users})

	return users, err
}

func RegisterUserProfileRoute(app *App) {
	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if !strings.HasPrefix(r.URL, "/@") {
			return true
		}

		name := r.URL[2:]

		if strings.Contains(name, "/") {
			return true
		}

		if strings.Contains(r.Headers.Get("Accept"), "json") {
			return true
		}

		user, err := app.Database.GetUser(name)

		if err != nil {
			return true
		}

		w.SetHeader("Content-Type", "text/html")
		w.Status(200)

		session := app.GetSession(r)

		RenderTemplate(app, w, "profile.html", map[string]interface{}{
			"me":              session.User.Name,
			"brandName":       BrandName,
			"brandSlug":       BrandSlug,
			"logoSVGURL":      LogoSVGURL,
			"type":            "Person",
			"name":            user.Name,
			"description":     template.HTML(app.HTMLSanitizer.Sanitize(user.Description)),
			"iconURL":         DefaultAvatar, // TODO: Implement profile avatars
			"created":         user.Created.Format("January 2, 2006"),
			"cancreateinvite": user.Role == "admin", // TODO: make this configurable
			"role":            user.Role,
		})

		return false
	})
}

func RegisterUserActorRoute(app *App) {
	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if !strings.HasPrefix(r.URL, "/@") {
			return true
		}

		name := r.URL[2:]

		if strings.Contains(name, "/") {
			return true
		}

		if !strings.Contains(r.Headers.Get("Accept"), "json") {
			return true
		}

		user, err := app.Database.GetUser(name)

		if err != nil {
			return true
		}

		w.JSON(200, map[string]interface{}{
			"@context": []string{
				"https://www.w3.org/ns/activitystreams",
				"https://w3id.org/security/v1",
			},
			"id":                GetURLFromName(app, name),
			"type":              "Person",
			"preferredUsername": name,
			"summary":           user.Description,
			"inbox":             GetInboxURLFromName(app, name),
			"icon":              DefaultAvatar,
			"publicKey": map[string]interface{}{
				"id":           GetKeyURLFromName(app, name),
				"owner":        GetURLFromName(app, name),
				"publicKeyPem": user.PublicKey, // TODO: key data
			},
		})

		return false
	})
}

func RegisterEditUserProfileRoute(app *App) {
	app.MVCApp.Use(func(r *mvc.Request, w *mvc.ResponseWriter) bool {
		if r.URL != "/edit-profile" {
			return true
		}

		if r.Method != "GET" && r.Method != "POST" {
			return true
		}

		session := app.GetSession(r)

		if !session.LoggedIn {
			w.SetHeader("Location", "/login")
			w.Status(303)
			return false
		}

		user, err := app.Database.GetUser(session.User.Name) // Refetch to make sure everything is up-to-date

		if err != nil {
			app.MVCApp.HandleError(500, r, w)
			return false
		}

		if r.Method == "POST" {
			r.RawRequest.(*http.Request).ParseForm()

			user.Description = r.RawRequest.(*http.Request).Form.Get("description")

			app.Database.UpdateUser(user)

			w.SetHeader("Location", GetURLFromName(app, session.User.Name))
			w.Status(303)

			return false
		}

		w.SetHeader("Content-Type", "text/html")
		w.Status(200)

		RenderTemplate(app, w, "editprofile.html", map[string]interface{}{
			"me":          session.User.Name,
			"brandName":   BrandName,
			"brandSlug":   BrandSlug,
			"logoSVGURL":  LogoSVGURL,
			"name":        user.Name,
			"description": template.HTML(user.Description),
			"iconURL":     DefaultAvatar, // TODO: Implement profile avatars
			"profileURL":  GetURLFromName(app, session.User.Name),
		})

		return false
	})
}